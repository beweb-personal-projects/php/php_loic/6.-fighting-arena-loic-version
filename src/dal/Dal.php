<?php
namespace Beweb\Td\Dal;

class Dal {
    private string $data_url;
    private mixed $data;

    public function __construct($data_url){
        $this->data_url = $data_url;
        $this->data = $this->get();
        $this->data = $this->setArray();
    }

    public function get():mixed {
        return file_get_contents($this->data_url); // Gets the file in string format.
    }

    public function isJson(): bool {
        return is_string($this->data) && is_array(json_decode($this->data, true)) ? true : false;
    }

    public function setArray(): mixed{
        if($this->isJson()) {
            $json_data = json_decode($this->data, true); // Converts the string to associative array.
            return $json_data;
        }
    }

    public function update(string $key, string $search, mixed $new_data):void {
        if(is_array($this->data)){
            foreach ($this->data as $i => $v) {
                foreach ($v as $k => $j) {
                    if($key === $k && $search === $v[$k]) {
                        $this->data[$i][$k] = $new_data;
                    }
                }
            }
        }

        $this->sortArray();
        $this->set();
    }

    public function sortArray():void {
        sort($this->data);
    }

    public function add($data):mixed {
        if(is_array($this->data)) {
            array_push($this->data, $data);
            $this->sortArray();
            $this->set();
        } else
            return false;
    }

    public function set():void {
        file_put_contents($this->data_url, json_encode($this->data));
    }
}