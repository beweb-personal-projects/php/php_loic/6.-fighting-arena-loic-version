<?php
namespace Beweb\Td\Dal;

use Beweb\Td\Models\Race;

class DaoRace extends Dao {

    private string $datasraces;
    public static $all_races = [];

    function __construct() {
        $this->datasraces = "./db/races.json";
    }

    function persist(mixed $data){
        file_put_contents($this->datasraces, json_encode($data));
    }

    function load(){

        $data_races = json_decode(file_get_contents($this->datasraces), true);
        global $current_race_name;
        
        //Iterate Each index of the array that contains the races
        foreach ($data_races as $i => $v) {
            $new_job = new Race;
            //Iterate Each Race
            foreach ($v as $k => $stats) {
                $current_race_name = $k;
                //Iterate Each Stats
                foreach ($stats as $k2 => $stat) {
                    //Example: human_object->modifier->pv = 100
                    $new_job->modifiers->$k2 = $stat;
                }
            }
            array_push(self::$all_races, [$current_race_name => $new_job]);
        }
    }
}