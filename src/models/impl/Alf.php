<?php 
namespace Beweb\Td\Models\Race\Impl;

use Beweb\Td\Models\Race;
use Beweb\Td\Models\Stats;

class Alf extends Race {

   


    function __construct(){
        parent::__construct();
        $this->modifiers->pv = 50;
        $this->modifiers->att = 2;
        $this->modifiers->def = 25;
    }

}